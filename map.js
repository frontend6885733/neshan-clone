import mapboxgl from "mapbox-gl";
import "mapbox-gl/dist/mapbox-gl.css";

mapboxgl.accessToken =
  "pk.eyJ1IjoibW9zYTU0NDUiLCJhIjoiY2p3a3ppcjY1MHA1ZTQ0cDk5MDU1eXNxOCJ9.psGVr3_kzZWMk--4Ojbhdw";

let map;
if (!map) {
  map = new mapboxgl.Map({
    container: "map",
    style: "mapbox://styles/mapbox/streets-v12",
    center: [59.578749, 36.313287],
    zoom: 10,
  });
}

export default map;
