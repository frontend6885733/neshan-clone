import mapboxgl from "mapbox-gl";
mapboxgl.accessToken =
  "pk.eyJ1IjoibW9zYTU0NDUiLCJhIjoiY2p3a3ppcjY1MHA1ZTQ0cDk5MDU1eXNxOCJ9.psGVr3_kzZWMk--4Ojbhdw";
import "./App.scss";

import { useSelector } from "react-redux";
import useMediaQuery from "./hooks/useMediaQuery";
import MainRoutingBtn from "./components/main-routing-btn/MainRoutingBtn";
import { selectIsRoutingOpen } from "./modules/routing/routingSelectors";
import RoutingApp from "./modules/routing/containers/routing-app/RoutingApp";
import InfoBoxApp from "./modules/info-box/containers/info-box-app/InfoBoxApp";

function App() {
  const { mobile } = useMediaQuery();

  const isRoutingOpen = useSelector(selectIsRoutingOpen);

  return (
    <div>
      {mobile && (
        <>
          {isRoutingOpen && <RoutingApp />}
          <MainRoutingBtn />
        </>
      )}
      <InfoBoxApp />
    </div>
  );
}

export default App;
