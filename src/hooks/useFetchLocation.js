import { useEffect, useState } from "react";
function useFetchLocation(query) {
  const [location, setLocation] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    async function fetchData() {
      try {
        setIsLoading(true);
        const response = await fetch(
          `https://api.neshan.org/v1/search?term=${query}&lat=36.313287&lng=59.578749`,
          {
            method: "GET",
            headers: { "Api-Key": "service.ca0fbe3c740a4461a17d7000d4461c76" },
          }
        );

        if (!response.ok) {
          throw new Error("Network response was not ok");
        }

        const data = await response.json();

        setLocation(
          data.items.map((loc) => ({
            title: loc.title,
            address: loc.address,
            type: loc.type,
            cord: { x: loc.location.x, y: loc.location.y },
          }))
        );
      } catch (error) {
        console.error("Error fetching location data:", error);
      } finally {
        setIsLoading(false);
      }
    }

    fetchData();
  }, [query]);

  return { location, isLoading };
}

export default useFetchLocation;
