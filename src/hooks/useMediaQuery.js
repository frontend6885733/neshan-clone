import { useEffect, useState } from "react";

function useMediaQuery(query = "(max-width: 768px)") {
  const [mobile, setMobile] = useState(window.matchMedia(query).matches);

  useEffect(() => {
    const matchQueryList = window.matchMedia(query);
    function handleChange(e) {
      setMobile(e.matches);
    }
    matchQueryList.addEventListener("change", handleChange);
    return () => {
      matchQueryList.removeEventListener("change", handleChange);
    };
  }, [query]);
  return { mobile };
}
export default useMediaQuery;
