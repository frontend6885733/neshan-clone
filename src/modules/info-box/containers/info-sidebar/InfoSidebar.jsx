import MainDescText from "../main-desc-text/MainDescText";
import MainDesc from "../../components/main-desc/MainDesc";
import "./InfoSidebar.scss";
import IconButtons from "../icon-buttons/IconButtons";
import TextIcons from "../text-icons/textIcons";
import CrowdedHours from "../../components/crowded-hours/CrowdedHours";
import RateBox from "../../components/rate-box/RateBox";
import LowerTextIcons from "../lower-text-icons/LowerTextIcons";
import CommentSection from "../comment-section/CommentSection";
import { closeRight } from "../../assets";
import { useDispatch, useSelector } from "react-redux";
import { selectIsInfoBoxOpen, selectSearchMarkerRef } from "../../infoBoxSelectors";
import { setIsOpenInfoBox } from "../../infoBoxActions";
function InfoSidebar() {
  const isInfoBoxOpen = useSelector(selectIsInfoBoxOpen);
  const searchMarkerRef = useSelector(selectSearchMarkerRef);
  const dispatch = useDispatch();
  const handleCLick = () => {
    dispatch(setIsOpenInfoBox(false));
    searchMarkerRef.remove();
  };
  return (
    <>
      <div className={`info-sidebar ${isInfoBoxOpen ? "open" : ""}`}>
        <MainDesc />
        <MainDescText />
        <div className="info-sidebar-container">
          <IconButtons />
        </div>
        <hr className="divider-info" />
        <div className="info-sidebar-container">
          <TextIcons />
          <CrowdedHours />
        </div>
        <RateBox />
        <div className="info-sidebar-container">
          <LowerTextIcons />
        </div>
        <div className="divider" />
        <div className="info-sidebar-container">
          <CommentSection />
        </div>
      </div>
      <button
        className="close-infobox-btn"
        style={!isInfoBoxOpen ? { display: "none" } : {}}
        onClick={handleCLick}
      >
        <img src={closeRight} alt="close" />
      </button>
    </>
  );
}

export default InfoSidebar;
