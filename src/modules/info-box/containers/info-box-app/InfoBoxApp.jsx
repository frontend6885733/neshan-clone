import { useDispatch, useSelector } from "react-redux";
import useMediaQuery from "../../../../hooks/useMediaQuery";
import InfoSidebar from "../info-sidebar/InfoSidebar";
import "./InfoBoxApp.scss";
import {
  selectCloseMainSearch,
  selectCloseSearchPage,
  selectIsInfoBoxOpen,
  selectIsMobileSearch,
  selectIsSearchOpen,
  selectSearchCords,
  selectSearchMarkerRef,
} from "../../infoBoxSelectors";
import MobileMainSearch from "../../../../components/mobile-main-search/MobileMainSearch";
import MainSearchSidebar from "../main-search-sidebar/MainSearchSidebar";
import SearchPage from "../../mobile-components/containers/search-page/SearchPage";
import {
  setIsOpenInfoBox,
  setSearchCords,
  setSearchMarkerRef,
} from "../../infoBoxActions";
import { useEffect } from "react";
import { createMarker } from "../../../../utils/createMarker";
import map from "../../../../../map";
import InfoBottomSheet from "../../mobile-components/containers/info-bottom-sheet/InfoBottomSheet";
import ExpandedSheet from "../../mobile-components/containers/expanded-sheet/ExpandedSheet";
import {
  selectIsRoutingOpen,
  selectShowSettingMenu,
} from "../../../routing/routingSelectors";
function InfoBoxApp() {
  const dispatch = useDispatch();
  const isMobileSearch = useSelector(selectIsMobileSearch);
  const isInfoBoxOpen = useSelector(selectIsInfoBoxOpen);
  const isSearchOpen = useSelector(selectIsSearchOpen);
  const searchCords = useSelector(selectSearchCords);
  const searchMarkerRef = useSelector(selectSearchMarkerRef);
  const closeSearchPage = useSelector(selectCloseSearchPage);
  const closeMainSearch = useSelector(selectCloseMainSearch);
  const showSettingMenu = useSelector(selectShowSettingMenu);
  const isRoutingOpen = useSelector(selectIsRoutingOpen);
  //custom hook
  const { mobile } = useMediaQuery();
  //functions
  const handleMarker = (markerRef, markerLoc, color) => {
    if (markerRef) markerRef.remove();
    if (markerLoc.length !== 0) {
      const newMarker = createMarker(markerLoc, color);
      newMarker.on("dragend", () => {
        const newMarkerLoc = newMarker.getLngLat();

        dispatch(setSearchCords([newMarkerLoc.lng, newMarkerLoc.lat]));
      });

      dispatch(setSearchMarkerRef(newMarker));
    }
  };
  const handleClick = (e) => {
    const clickedMarkerLoc = [e.lngLat.lng, e.lngLat.lat];
    dispatch(setSearchCords(clickedMarkerLoc));
  };
  useEffect(() => {
    if (!isRoutingOpen && (isInfoBoxOpen || isSearchOpen || mobile)) {
      map.on("click", handleClick);
      return () => map.off("click", handleClick);
    }
  }, [isSearchOpen, isInfoBoxOpen, isRoutingOpen]);
  useEffect(() => {
    //open info box
    if (searchCords.length != 0) {
      dispatch(setIsOpenInfoBox(true));
    }
    handleMarker(searchMarkerRef, searchCords, "#f05e60");
  }, [searchCords]);

  return (
    <>
      {mobile && (
        <>
          {isMobileSearch && !closeSearchPage && <SearchPage />}
          <InfoBottomSheet />
          {!isInfoBoxOpen && !showSettingMenu && <MobileMainSearch />}
        </>
      )}
      {!mobile && ( //desktop
        <>
          <MainSearchSidebar />
          <InfoSidebar />
        </>
      )}
    </>
  );
}

export default InfoBoxApp;
