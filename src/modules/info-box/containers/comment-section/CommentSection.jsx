import { avatar1, avatar2, avatar3 } from "../../assets";
import Comment from "../../components/comment/Comment";
import "./CommentSection.scss";

function CommentSection() {
  return (
    <div className="comment-section">
      <h2 className="comments-title">نظر کاربران درباره این مکان</h2>
      <div className="all-comments">
        <Comment
          avatar={avatar1}
          username="علی رضاییان"
          date="۱۲ اردیبهشت ۱۳۹۹"
          text="بهترین رستورانی است که در این محله می شناسم."
        />
        <Comment
          avatar={avatar2}
          username="علی رضاییان"
          date="۱۲ اردیبهشت ۱۳۹۹"
          text="بهترین رستورانی است که در این محله می شناسم فقط مشکلش اینه که یکم فضاش کوچیکه"
        />
        <Comment
          avatar={avatar3}
          username="علی رضاییان"
          date="۲ روز پیش"
          text="
          لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از
          طراحان گرافیک است، چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که
          لازم است، و برای شرایط فعلی تکنولوژی مورد نیاز، و کاربردهای متنوع با هدف بهبود
          ابزارهای کاربردی می باشد،"
        />
      </div>
      <div className="button-comment-wrapper">
        <button className="show-more-comments-btn">سایر نظرات</button>
      </div>
    </div>
  );
}

export default CommentSection;
