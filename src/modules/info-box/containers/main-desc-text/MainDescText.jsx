import { carBlue } from "../../../routing/assets";
import { restaurantIcon, vertLine } from "../../assets";
import SidebarTextField from "../../components/sidebar-text-field/SidebarTextField";
import "./MainDescText.scss";

function MainDescText() {
  return (
    <div className="main-desc-text">
      <div className="main-desc-titles">
        <img src={restaurantIcon} alt="" />
        <SidebarTextField />
      </div>
      <div className="metrics-info">
        <img src={carBlue} alt="" />
        <div className="duration-info">
          <div className="num-metric-info">
            <span className="num-info">11</span>
            <span className="metric-info">دقیقه</span>
          </div>
          <img src={vertLine} alt="" />
          <div className="num-metric-info">
            <span className="num-info">22</span>
            <span className="metric-info">کیلومتر</span>
          </div>
        </div>
      </div>
    </div>
  );
}

export default MainDescText;
