import { useDispatch, useSelector } from "react-redux";
import { benzin, gaz, rest, restaurant, revealerArrow, wc } from "../../assets";
import ServiceBtn from "../../components/service-btn/ServiceBtn";
import "./ServiceBtns.scss";
import { setSlide } from "../../infoBoxActions";
import { selectSlide } from "../../infoBoxSelectors";
import useMediaQuery from "../../../../hooks/useMediaQuery";

function ServiceBtns() {
  const mobile = useMediaQuery();
  const slide = useSelector(selectSlide);
  const dispatch = useDispatch();
  const handleClick = () => {
    if (slide === "right") dispatch(setSlide("left"));
    else dispatch(setSlide("right"));
  };

  const slider = slide === "right" ? "right" : "left";

  return (
    <div className="service-btns">
      <div className={`services-container ${slider}`}>
        <ServiceBtn logo={gaz} text="پمپ گاز" />
        <ServiceBtn logo={benzin} text="پمپ بنزین" />
        <ServiceBtn logo={wc} text="سرویس بهداشتی" />
        <ServiceBtn logo={restaurant} text="رستوران" />
        <ServiceBtn logo={rest} text="سایر" />
      </div>

      <button className={`revealer-btn ${slider}`} onClick={handleClick}>
        <img src={revealerArrow} alt="show more" />
      </button>
    </div>
  );
}

export default ServiceBtns;
