import { useSelector } from "react-redux";
import { blueLocation, clock, phone, phonePlus, planet } from "../../assets";
import TextIcon from "../../components/text-icon/TextIcon";
import WorkHours from "../../components/work-hours/WorkHours";
import useReverseGeo from "../../hooks/useReverseGeo";
import "./TextIcons.scss";
import { selectSearchCords } from "../../infoBoxSelectors";

function TextIcons() {
  const searchCords = useSelector(selectSearchCords);
  const { address } = useReverseGeo(searchCords);
  return (
    <div className="text-icons">
      <TextIcon
        icon={blueLocation}
        text={address.formatted_address || "آدرسی یافت نشد"}
      />
      <TextIcon icon={phone} text="تلفن تماس : ۰۲۱۸۳۳۷۲۲۲" />
      <TextIcon icon={phonePlus} text="افزودن تلفن تماس برای این مکان" color="#1976D2" />
      <TextIcon icon={planet} text="آدرس سایت www.mobinet.com" />
      <WorkHours />
    </div>
  );
}

export default TextIcons;
