import "./MainSearchSidebar.scss";
import { useSelector } from "react-redux";
import { selectIsRoutingOpen } from "../../../routing/routingSelectors";
import { selectIsInfoBoxOpen, selectIsSearchOpen } from "../../infoBoxSelectors";
import SearchInput from "../../components/search-input/SearchInput";
import RoutingApp from "../../../routing/containers/routing-app/RoutingApp";
import ServiceBtns from "../service-buttons/ServiceBtns";
import SearchPlaces from "../search-places/SearchPlaces";
import SearchedLocations from "../searched-locations/SearchedLocations";

function MainSearchSidebar() {
  const isRoutingOpen = useSelector(selectIsRoutingOpen);
  const isSearchOpen = useSelector(selectIsSearchOpen);
  const isInfoBoxOpen = useSelector(selectIsInfoBoxOpen);
  return (
    <div
      className={`side-module-container ${isSearchOpen ? "open" : ""} ${
        isRoutingOpen ? "route-open-s" : ""
      } ${isInfoBoxOpen ? "width-close" : ""}`}
    >
      {!isRoutingOpen && <SearchInput />}
      {isRoutingOpen && <RoutingApp />}
      {isSearchOpen && (
        <>
          <ServiceBtns />
          <SearchPlaces />
          <div className="divider search-page" />
          <SearchedLocations />
        </>
      )}
    </div>
  );
}

export default MainSearchSidebar;
