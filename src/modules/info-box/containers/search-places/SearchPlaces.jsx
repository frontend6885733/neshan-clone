import { briefcase, star } from "../../../routing/assets";
import Place from "../../../routing/components/place/Place";
import { homePlus } from "../../assets";
import "./SearchPlaces.scss";

function SearchPlaces() {
  return (
    <div className="places-search">
      <Place icon={homePlus} text="خانه" />
      <div className="vert-search-pl" />
      <Place icon={briefcase} text="محل کار" />
      <div className="vert-search-pl" />
      <Place icon={star} text="مکان های شخصی" />
    </div>
  );
}

export default SearchPlaces;
