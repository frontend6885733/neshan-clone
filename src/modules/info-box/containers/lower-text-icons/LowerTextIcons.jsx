import { avatar, comment, edit, info, target } from "../../assets";
import TextIcon from "../../components/text-icon/TextIcon";
import "./LowerTextIcons.scss";

function LowerTextIcons() {
  return (
    <div>
      <TextIcon icon={info} text="گزارش اشکال نقشه" />
      <TextIcon icon={target} text="مختصات جغرافیایی" />
      <div className="edit-info">
        <img src={edit} alt="" />
        <div>
          <p>ویرایش اطلاعات یا گزارش تعطیلی مکان</p>
          <span>ویرایش شما در حال بررسی است</span>
        </div>
      </div>
      <TextIcon icon={avatar} text="آخرین ویرایش توسط: mostafa-javadi" />
      <div className="already-rated">
        <img src={comment} alt="" />
        <div>
          <p>شما قبلا درباره این مکان نظر دادید.</p>
          <span> ویرایش نظر و امتیاز</span>
        </div>
      </div>
      <div className="red-box">
        <p>
          زا موهفمان یگداس دیلوت اب یگتخاس نتم موسپیا مرول .تسا کیفارگ ناحارط زا هدافتسا
          اب و پاچ تعنص
        </p>
      </div>
    </div>
  );
}

export default LowerTextIcons;
