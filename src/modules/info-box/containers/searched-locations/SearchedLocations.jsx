import { useSelector } from "react-redux";
import useFetchLocation from "../../../../hooks/useFetchLocation";
import SearchedLocation from "../../components/searched-location/SearchedLocation";
import { selectSearchQuery } from "../../infoBoxSelectors";
import "./SearchedLocations.scss";

function SearchedLocations() {
  const searchQuery = useSelector(selectSearchQuery);
  const { location, isLoading } = useFetchLocation(searchQuery);

  return (
    <div className="info-searched-locations">
      {location?.map((loc, i) => (
        <SearchedLocation
          key={i}
          title={loc.title}
          subtitle={loc.address}
          type={loc.type}
          cords={loc.cord}
        />
      ))}
    </div>
  );
}

export default SearchedLocations;
