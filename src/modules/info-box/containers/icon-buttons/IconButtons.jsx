import IconButton from "../../components/icon-button/IconButton";
import "./IconButtons.scss";
import { starPlus, routingIcon } from "../../assets";
function IconButtons() {
  return (
    <div className="icon-buttons">
      <IconButton icon={routingIcon} text="مسیریابی" />
      <IconButton icon={starPlus} text="معابر ترافیک" />
      <IconButton icon={starPlus} text="دوربین ترافیکی" />
      <IconButton icon={starPlus} text="رویداد ها" />
    </div>
  );
}

export default IconButtons;
