import { cameraPlus, edit, shop, target } from "../../../assets";
import ExpandedTextIcon from "../../components/expanded-text-icon/ExpandedTextIcon";
import "./LowerExpandedTextIcons.scss";

function LowerExpandedTextIcons() {
  return (
    <div className="lower-exp-icons">
      <ExpandedTextIcon
        text="افزودن تصویر برای این مکان"
        icon={cameraPlus}
        textColor="var(--primary-color)"
      />
      <ExpandedTextIcon text="اصلاح/تکمیل اطلاعات این مکان" icon={edit} />
      <ExpandedTextIcon text="مدیر کسب و کار" icon={shop} />
      <ExpandedTextIcon text="مختصات جغرافیایی" icon={target} />
      <ExpandedTextIcon
        text="آخرین ویرایش توسط: علیرضا زارعی"
        icon={target}
        textColor="var(--primary-color)"
      />
    </div>
  );
}

export default LowerExpandedTextIcons;
