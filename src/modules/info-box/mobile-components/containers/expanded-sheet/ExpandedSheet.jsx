import { closeInfo } from "../../../assets";
import RateBox from "../../../components/rate-box/RateBox";
import CommentSection from "../../../containers/comment-section/CommentSection";
import { setExpandMenu } from "../../../infoBoxActions";
import ExpandedSheetBtns from "../expanded-sheet-btns/ExpandedSheetBtns";
import ExpandedSheetDesc from "../expanded-sheet-desc/ExpandedSheetDesc";
import ExpandedTextIcons from "../expanded-text-icons/ExpandedTextIcons";
import LowerExpandedTextIcons from "../lower-expanded-text-icons/LowerExpandedTextIcons";
import "./ExpandedSheet.scss";
import { useDispatch } from "react-redux";
function ExpandedSheet() {
  const dispatch = useDispatch();
  return (
    <div className="expanded-sheet">
      <div className="mock-img-container">
        <div className="mock-img" />
        <div className="mock-img" />
        <button
          className="close-bttm-sheet"
          onClick={() => dispatch(setExpandMenu(false))}
        >
          <img src={closeInfo} alt="close" />
        </button>
      </div>
      <div className="expanded-sheet-container">
        <ExpandedSheetDesc />
        <ExpandedSheetBtns />
      </div>
      <div className="exp-divider" />
      <ExpandedTextIcons />
      <RateBox />
      <LowerExpandedTextIcons />
      <div className="exp-divider lower" />
      <div className="expanded-sheet-container comments">
        <CommentSection />
      </div>
    </div>
  );
}

export default ExpandedSheet;
