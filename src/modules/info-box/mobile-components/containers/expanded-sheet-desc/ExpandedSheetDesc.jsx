import { useSelector } from "react-redux";
import { car } from "../../../../routing/assets";
import { blueOutlinedCam, restaurantIcon, stars } from "../../../assets";
import { selectSearchCords } from "../../../infoBoxSelectors";
import "./ExpandedSheetDesc.scss";
import useReverseGeo from "../../../hooks/useReverseGeo";

function ExpandedSheetDesc() {
  const searchCords = useSelector(selectSearchCords);
  const { address } = useReverseGeo(searchCords);
  return (
    <div className="expanded-sheet-desc">
      <button className="add-img-btn">
        <span>افزودن تصویر</span>
        <img src={blueOutlinedCam} alt="" />
      </button>
      <div className="exp-top">
        <img src={restaurantIcon} alt="" />
        <div className="exp-text">
          <p>
            {address.place ||
              address.neighbourhood ||
              address.county ||
              "مکانی یافت نشد!"}
          </p>
          <span>{address.village || address.district || "یافت نشد"}</span>
        </div>
      </div>
      <div className="exp-sub">
        <div className="exp-metrics">
          <img src={car} alt="" />
          <div className="bottom-info-durations">
            <div>
              <span className="number">۱۲</span> <span>دقیقه</span>
            </div>
            <div>
              <span className="number">۸/۳</span> <span>کیلومتر</span>
            </div>
          </div>
        </div>
        <div className="bottom-sheet-rating expanded">
          <span>۳/۴</span>
          <img src={stars} alt="" />
          <span>۸۳ رای</span>
        </div>
      </div>
    </div>
  );
}

export default ExpandedSheetDesc;
