import "./BottomSheetBtns.scss";
import BottomSheetBtn from "../../components/bottom-sheet-btn/BottomSheetBtn";
import { blueStar, letsGo, miniDirection } from "../../../assets";

function BottomSheetBtns() {
  return (
    <div className="bottom-sheet-btns">
      <BottomSheetBtn
        text="مسیریابی"
        icon={miniDirection}
        color="var(--primary-color)"
        textColor="white"
      />
      <BottomSheetBtn text="بزن بریم" icon={letsGo} />
      <BottomSheetBtn text="ذخیره سازی" icon={blueStar} />
    </div>
  );
}

export default BottomSheetBtns;
