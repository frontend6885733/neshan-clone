import "./ExpandedSheetBtns.scss";
import IconButton from "../../../components/icon-button/IconButton";
import { callIcon, routingIcon, saveIcon, shareIcon } from "../../../assets";

function ExpandedSheetBtns() {
  return (
    <div className="expanded-sheet-btns">
      <IconButton icon={routingIcon} text="مسیریابی" color="var(--primary-color)" />
      <IconButton icon={callIcon} text="تماس" color="var(--primary-color)" />
      <IconButton icon={saveIcon} text="ذخیره" color="var(--primary-color)" />
      <IconButton icon={shareIcon} text="ارسال" color="var(--primary-color)" />
    </div>
  );
}

export default ExpandedSheetBtns;
