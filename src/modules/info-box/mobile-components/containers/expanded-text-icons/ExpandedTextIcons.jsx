import { useSelector } from "react-redux";
import { blueLocation, phone, planet } from "../../../assets";
import useReverseGeo from "../../../hooks/useReverseGeo";
import { selectSearchCords } from "../../../infoBoxSelectors";
import ExpandedTextIcon from "../../components/expanded-text-icon/ExpandedTextIcon";
import "./ExpandedTextIcons.scss";

function ExpandedTextIcons() {
  const searchCords = useSelector(selectSearchCords);
  const { address } = useReverseGeo(searchCords);
  return (
    <div className="expanded-text-icons">
      <ExpandedTextIcon
        text={address.formatted_address || "آدرسی یافت نشد"}
        icon={blueLocation}
      />
      <ExpandedTextIcon
        text="ساعات کاری:"
        moreText="باز است - ۹ تا ۱۷"
        icon={blueLocation}
        moreTextColor="var(--primary-color)"
      />
      <ExpandedTextIcon
        text="تلفن تماس:"
        moreText="۰۹۱۲۳۴۳۳۴"
        moreTextColor="var(--primary-color)"
        icon={phone}
      />
      <ExpandedTextIcon
        text="وبسایت:"
        icon={planet}
        moreText="salamat.gov.ir"
        moreTextColor="var(--primary-color)"
      />
    </div>
  );
}

export default ExpandedTextIcons;
