import { useDispatch, useSelector } from "react-redux";
import SearchPlaces from "../../../containers/search-places/SearchPlaces";
import ServiceBtns from "../../../containers/service-buttons/ServiceBtns";
import "./SearchPage.scss";
import SearchedLocations from "../../../containers/searched-locations/SearchedLocations";
import { back } from "../../../../routing/assets";
import { setIsMobileSearch, setSearchQuery } from "../../../infoBoxActions";

function SearchPage() {
  const dispatch = useDispatch();

  return (
    <div className="mobile-search-page">
      <div className="search-page-input-wrapper">
        <button onClick={() => dispatch(setIsMobileSearch(false))}>
          <img src={back} alt="" />
        </button>
        <input
          placeholder="جستجو"
          className="mobile-main-input-p"
          type="search"
          autoComplete="off"
          onChange={(e) => dispatch(setSearchQuery(e.target.value))}
        />
      </div>
      <ServiceBtns />
      <SearchPlaces />
      <SearchedLocations />
    </div>
  );
}

export default SearchPage;
