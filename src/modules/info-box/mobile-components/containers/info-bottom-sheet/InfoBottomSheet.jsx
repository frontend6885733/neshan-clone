import { useDispatch, useSelector } from "react-redux";
import { closeInfo, placeImg } from "../../../assets";
import MainMobileDesc from "../../components/main-mobile-desc/MainMobileDesc";
import "./InfoBottomSheet.scss";
import {
  setCloseBottomSheet,
  setCloseSearchPage,
  setIsOpenInfoBox,
} from "../../../infoBoxActions";
import {
  selectCloseBottomSheet,
  selectExpandMenu,
  selectIsInfoBoxOpen,
  selectSearchMarkerRef,
} from "../../../infoBoxSelectors";
import ExpandedSheet from "../expanded-sheet/ExpandedSheet";
import { selectIsRoutingOpen } from "../../../../routing/routingSelectors";

function InfoBottomSheet() {
  const dispatch = useDispatch();
  const closeBottomSheet = useSelector(selectCloseBottomSheet);
  const searchMarkerRef = useSelector(selectSearchMarkerRef);
  const isInfoBoxOpen = useSelector(selectIsInfoBoxOpen);
  const expandMenu = useSelector(selectExpandMenu);
  const isRoutingOpen = useSelector(selectIsRoutingOpen);
  const handleCloseClick = () => {
    dispatch(setCloseBottomSheet(true));
    dispatch(setCloseSearchPage(false));
    dispatch(setIsOpenInfoBox(false));
    searchMarkerRef.remove();
  };

  //styles
  const hide = closeBottomSheet ? "hide" : "";
  const show = isInfoBoxOpen ? "show" : "";
  const expand = expandMenu ? "expand" : "minimize";
  return (
    <>
      <div
        className={`info-bottom-sheet ${hide} ${show} ${expand}`}
        style={isRoutingOpen ? { display: "none" } : {}}
      >
        <div
          className="narrow-img-container"
          style={expandMenu ? { display: "none" } : {}}
        >
          <img className="narrow-img" src={placeImg} alt="" />
        </div>
        {expandMenu ? <ExpandedSheet /> : <MainMobileDesc />}
      </div>

      <button
        style={!isInfoBoxOpen || expandMenu || isRoutingOpen ? { display: "none" } : {}}
        className="close-info-bs"
        onClick={handleCloseClick}
      >
        <img src={closeInfo} alt="" />
      </button>
    </>
  );
}

export default InfoBottomSheet;
