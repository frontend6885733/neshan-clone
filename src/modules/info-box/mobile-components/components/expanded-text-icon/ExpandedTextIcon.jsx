import "./ExpandedTextIcon.scss";

function ExpandedTextIcon({
  icon,
  text,
  moreText = "",
  textColor = "#262626",
  moreTextColor = "#262626",
}) {
  return (
    <div className="expanded-text-icon">
      <img src={icon} alt="" />
      <div>
        <p style={{ color: textColor }}>{text}</p>
        <span style={{ color: moreTextColor }}>{moreText}</span>
      </div>
    </div>
  );
}

export default ExpandedTextIcon;
