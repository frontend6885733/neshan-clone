import { useDispatch, useSelector } from "react-redux";
import { car } from "../../../../routing/assets";
import { grayLoc, restaurantIcon, stars, upMore } from "../../../assets";
import useReverseGeo from "../../../hooks/useReverseGeo";
import { selectSearchCords } from "../../../infoBoxSelectors";
import BottomSheetBtns from "../../containers/bottom-sheet-btns/BottomSheetBtns";
import "./MainMobileDesc.scss";
import { setExpandMenu } from "../../../infoBoxActions";

function MainMobileDesc() {
  const dispatch = useDispatch();
  const searchCords = useSelector(selectSearchCords);
  const { address } = useReverseGeo(searchCords);
  const handleClick = () => {
    dispatch(setExpandMenu(true));
  };
  return (
    <div className="main-mobile-desc">
      <div className="main-mobile-desc-title">
        <div className="sheet-text-icon">
          <img src={restaurantIcon} alt="icon" />
          <p>
            {address.place ||
              address.neighbourhood ||
              address.county ||
              "مکانی یافت نشد!"}
          </p>
        </div>
        <button className="show-btn-bottom-sheet" onClick={handleClick}>
          <span>بیشتر</span>
          <img src={upMore} alt="" />
        </button>
      </div>
      <div className="bottom-sheet-sub">
        <span>{address.village || address.district || "یافت نشد"}</span> -{" "}
        <span>باز است</span> : <span>۷:۳۰ تا ۹</span>
      </div>
      <div className="bottom-sheet-rating">
        <span>۳/۴</span>
        <img src={stars} alt="" />
        <span>۸۳ رای</span>
      </div>
      <div>
        <div className="bottom-icon-txt">
          <img src={grayLoc} alt="" />
          <span>{address.formatted_address || "آدرسی یافت نشد"}</span>
        </div>
        <div className="bottom-icon-txt">
          <img src={car} alt="" />
          <div className="bottom-info-durations">
            <div>
              <span className="number">۱۲</span> <span>دقیقه</span>
            </div>
            <div>
              <span className="number">۸/۳</span> <span>کیلومتر</span>
            </div>
          </div>
        </div>
      </div>
      <BottomSheetBtns />
    </div>
  );
}

export default MainMobileDesc;
