import "./BottomSheetBtn.scss";

function BottomSheetBtn({
  text,
  icon,
  color = "transparent",
  textColor = "var(--primary-color)",
}) {
  return (
    <button className="bottom-sheet-btn" style={{ background: color }}>
      <img src={icon} alt="" />
      <span style={{ color: textColor }}>{text}</span>
    </button>
  );
}

export default BottomSheetBtn;
