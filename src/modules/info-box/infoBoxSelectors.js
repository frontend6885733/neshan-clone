export const selectIsSearchOpen = (state) => state.infoUI.isSearchOpen;
export const selectIsMobileSearch = (state) => state.infoUI.isMobileSearch;
export const selectSlide = (state) => state.infoUI.slide;
export const selectIsInfoBoxOpen = (state) => state.infoUI.isInfoBoxOpen;
export const selectCloseSearchPage = (state) => state.infoUI.closeSearchPage;
export const selectCloseBottomSheet = (state) => state.infoUI.closeBottomSheet;
export const selectCloseMainSearch = (state) => state.infoUI.closeMainSearch;
export const selectExpandMenu = (state) => state.infoUI.expandMenu;
//non-ui
export const selectSearchQuery = (state) => state.info.searchQuery;
export const selectSearchCords = (state) => state.info.searchCords;
export const selectSearchMarkerRef = (state) => state.info.searchMarkerRef;
