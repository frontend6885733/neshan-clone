import { useEffect, useState } from "react";
function useReverseGeo(searchCords) {
  const [address, setAddress] = useState([]);

  useEffect(() => {
    async function fetchData() {
      try {
        if (searchCords.length != 0) {
          const response = await fetch(
            `https://api.neshan.org/v5/reverse?lat=${searchCords[1]}&lng=${searchCords[0]}`,
            {
              method: "GET",
              headers: { "Api-Key": "service.ca0fbe3c740a4461a17d7000d4461c76" },
            }
          );

          if (!response.ok) {
            throw new Error("Network response was not ok");
          }

          const data = await response.json();
          setAddress(data);
        }
      } catch (error) {
        console.error("Error fetching location data:", error);
      }
    }

    fetchData();
  }, [searchCords]);

  return { address };
}

export default useReverseGeo;
