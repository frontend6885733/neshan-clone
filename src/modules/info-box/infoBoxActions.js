export const setIsSearchOpen = (value) => {
  return { type: "OPEN_SEARCH_BOX", payload: value };
};
export const setIsMobileSearch = (value) => {
  return { type: "OPEN_MOBILE_SEARCH", payload: value };
};
export const setSlide = (value) => {
  return { type: "SLIDE", payload: value };
};
export const setIsOpenInfoBox = (value) => {
  return { type: "OPEN_INFOBOX", payload: value };
};
export const setCloseSearchPage = (value) => {
  return { type: "CLOSE_MOBILE_SEARCH_PAGE", payload: value };
};
export const setCloseBottomSheet = (value) => {
  return { type: "CLOSE_INFO_BOTTOM_SHEET", payload: value };
};
export const setCloseMainSearch = (value) => {
  return { type: "SET_SHOW_MAIN_SEARCH_INPUT", payload: value };
};
export const setExpandMenu = (value) => {
  return { type: "EXPAND_MENU", payload: value };
};
//non-ui
export const setSearchQuery = (value) => {
  return { type: "SET_SEARCH_QUERY", payload: value };
};
export const setSearchCords = (value) => {
  return { type: "SET_SEARCH_CORDS", payload: value };
};
export const setSearchMarkerRef = (value) => {
  return { type: "SET_SEARCH_MARKER_REF", payload: value };
};
