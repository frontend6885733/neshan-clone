import { useSelector } from "react-redux";
import { stars } from "../../assets";
import useReverseGeo from "../../hooks/useReverseGeo";
import "./SidebarTextField.scss";
import { selectSearchCords } from "../../infoBoxSelectors";

function SidebarTextField() {
  const searchCords = useSelector(selectSearchCords);
  const { address } = useReverseGeo(searchCords);
  return (
    <div className="sidebar-text-field">
      <p>
        {address.place || address.neighbourhood || address.county || "مکانی یافت نشد!"}
      </p>
      <span className="main-text-desc-subtitle">
        {address.village || address.district}
      </span>
      <div className="text-rate-container">
        <span>۳/۴</span>
        <img src={stars} alt="" />
        <span>۸۳ رای</span>
      </div>
    </div>
  );
}

export default SidebarTextField;
