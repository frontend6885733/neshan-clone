import { useDispatch } from "react-redux";
import { city } from "../../../routing/assets";
import "./SearchedLocation.scss";
import {
  setCloseSearchPage,
  setIsOpenInfoBox,
  setSearchCords,
} from "../../infoBoxActions";

function SearchedLocation({ title, subtitle, type, cords }) {
  const dispatch = useDispatch();
  const handleClick = () => {
    dispatch(setSearchCords([cords.x, cords.y]));
    dispatch(setCloseSearchPage(true));
    dispatch(setIsOpenInfoBox(true));
  };
  return (
    <div className="search-loc-result-wrapper-info" onClick={handleClick}>
      <img src={city} alt="" />
      <div className="search-loc-result-container-info">
        <p className="title-loc-result-info">{title}</p>
        <span className="subtitle-loc-result-info">{subtitle}</span>
        <span className="type-loc-result-info">{type}</span>
      </div>
    </div>
  );
}

export default SearchedLocation;
