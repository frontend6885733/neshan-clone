import { commentStars } from "../../assets";
import "./Comment.scss";

function Comment({ avatar, username, date, text }) {
  return (
    <div className="comment">
      <img src={avatar} alt="avatar" className="avatar-comment" />
      <div className="comment-content">
        <span className="username-comment">{username}</span>
        <div className="stars-section">
          <img src={commentStars} alt="ratings" />
          <span className="date-comment">{date}</span>
        </div>
        <p>{text}</p>
      </div>
    </div>
  );
}

export default Comment;
