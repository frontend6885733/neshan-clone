import { clock } from "../../assets";
import "./WorkHours.scss";

function WorkHours() {
  return (
    <div className="work-hours-container">
      <img src={clock} alt="" />
      <div className="work-hours-text">
        <div>
          <p>ساعات کاری: ۹ تا ۱۷</p>
          <p className="colored-open">باز است</p>
        </div>
        <p className="colored-corona">
          شیوع کرونا ممکن است در ساعات کاری تاثیر داشته باشد
        </p>
      </div>
    </div>
  );
}

export default WorkHours;
