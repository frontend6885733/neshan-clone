import "./SearchInput.scss";
import { useDispatch, useSelector } from "react-redux";
import { setIsSearchOpen, setSearchQuery } from "../../infoBoxActions";
import { selectCloseMainSearch, selectIsSearchOpen } from "../../infoBoxSelectors";
import { hamMenu, navigation, search } from "../../assets";
import { setIsRoutingOpen } from "../../../routing/routingActions";
import { x } from "../../../routing/assets";
function SearchInput() {
  const dispatch = useDispatch();
  const isSearchOpen = useSelector(selectIsSearchOpen);

  return (
    <div className={`main-input-wrapper ${isSearchOpen ? "gray-field" : ""}`}>
      <img src={hamMenu} alt="menu" />
      <input
        className={`info-box-search ${isSearchOpen ? "gray-input" : ""}`}
        type="text"
        placeholder="جستجو"
        onFocus={() => dispatch(setIsSearchOpen(true))}
        onChange={(e) => dispatch(setSearchQuery(e.target.value))}
      />
      <img src={search} alt="" />
      <hr className="main-hor-line" />
      {!isSearchOpen && (
        <button
          className="navigation-icon-btn"
          onClick={() => dispatch(setIsRoutingOpen(true))}
        >
          <img src={navigation} alt="" />
        </button>
      )}
      {isSearchOpen && (
        <button className="close-i-btn" onClick={() => dispatch(setIsSearchOpen(false))}>
          <img src={x} alt="close" />
        </button>
      )}
    </div>
  );
}

export default SearchInput;
