import { placeImg } from "../../assets";
import "./MainDesc.scss";

function MainDesc() {
  return (
    <div className="info-main-desc">
      <div className="image-cont">
        <img
          className="image-cont-img"
          src={placeImg}
          alt="the image of the searched location"
        />
      </div>
    </div>
  );
}

export default MainDesc;
