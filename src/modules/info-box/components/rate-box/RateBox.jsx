import "./RateBox.scss";
import { bigStars } from "../../assets";

function RateBox() {
  return (
    <div className="rate-box">
      <p>به این مکان امتیاز دهید</p>
      <img src={bigStars} alt="" />
      <input type="text" placeholder="نظرتان را درباره این مکان بنویسید" />
    </div>
  );
}

export default RateBox;
