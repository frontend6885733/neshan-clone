import { bigChart, chartIcon } from "../../assets";
import "./CrowdedHours.scss";

function CrowdedHours() {
  return (
    <div className="crowded-hours">
      <div>
        <img src={chartIcon} alt="" />
        <p>ساعات پر تردد</p>
      </div>
      <img src={bigChart} alt="" />
    </div>
  );
}

export default CrowdedHours;
