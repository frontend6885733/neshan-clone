import "./IconButton.scss";

function IconButton({ icon, text, color = "black" }) {
  return (
    <div className="icon-button">
      <img src={icon} alt="navigation" />
      <span style={{ color: color }}>{text}</span>
    </div>
  );
}

export default IconButton;
