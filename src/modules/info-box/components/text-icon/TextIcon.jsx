import "./TextIcon.scss";

function TextIcon({ icon, text, color = "black" }) {
  return (
    <div className="text-icon">
      <img src={icon} alt="" />
      <p style={{ color: color }} color={color}>
        {text}
      </p>
    </div>
  );
}

export default TextIcon;
