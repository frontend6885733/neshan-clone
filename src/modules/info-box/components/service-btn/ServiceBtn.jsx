import "./ServiceBtn.scss";

function ServiceBtn({ logo, text }) {
  return (
    <button className="service-btn">
      <img src={logo} alt="" />
      <span>{text}</span>
    </button>
  );
}

export default ServiceBtn;
