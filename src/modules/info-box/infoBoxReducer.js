const initialState = {
  searchQuery: "",
  searchCords: [],
  searchMarkerRef: null,
};
function infoBoxReducer(state = initialState, action) {
  switch (action.type) {
    case "SET_SEARCH_QUERY":
      return { ...state, searchQuery: action.payload };
    case "SET_SEARCH_CORDS":
      return { ...state, searchCords: action.payload };
    case "SET_SEARCH_MARKER_REF":
      return { ...state, searchMarkerRef: action.payload };
    default:
      return state;
  }
}
export default infoBoxReducer;
