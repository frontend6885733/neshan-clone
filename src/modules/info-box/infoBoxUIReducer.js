const initialState = {
  isSearchOpen: false,
  isMobileSearch: false,
  slide: "left",
  isInfoBoxOpen: false, //change
  closeSearchPage: false,
  closeBottomSheet: false,
  closeMainSearch: true,
  expandMenu: false, //change
};
function infoBoxUIReducer(state = initialState, action) {
  switch (action.type) {
    case "OPEN_SEARCH_BOX":
      return { ...state, isSearchOpen: action.payload };
    case "OPEN_MOBILE_SEARCH":
      return { ...state, isMobileSearch: action.payload };
    case "SLIDE":
      return { ...state, slide: action.payload };
    case "OPEN_INFOBOX":
      return { ...state, isInfoBoxOpen: action.payload };
    case "CLOSE_MOBILE_SEARCH_PAGE":
      return { ...state, closeSearchPage: action.payload };
    case "CLOSE_INFO_BOTTOM_SHEET":
      return { ...state, closeBottomSheet: action.payload };
    case "SET_SHOW_MAIN_SEARCH_INPUT":
      return { ...state, closeMainSearch: action.payload };
    case "EXPAND_MENU":
      return { ...state, expandMenu: action.payload };
    default:
      return state;
  }
}
export default infoBoxUIReducer;
