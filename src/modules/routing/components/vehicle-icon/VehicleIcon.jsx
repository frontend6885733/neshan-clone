import { useDispatch, useSelector } from "react-redux";
import "./VehicleIcon.scss";
import { selectVehicleType } from "../../routingSelectors";
import { setVehicleType } from "../../routingActions";
function VehicleIcon({ src, type, coloredSrc }) {
  const dispatch = useDispatch();
  const vehicleType = useSelector(selectVehicleType);
  const handleClick = () => {
    dispatch(setVehicleType(type));
  };

  //style

  return (
    <div
      className={`vehicle-icon-container ${vehicleType === type ? "active" : ""}`}
      onClick={handleClick}
    >
      <img src={vehicleType === type ? coloredSrc : src} alt={type} />
      <span>{type}</span>
    </div>
  );
}

export default VehicleIcon;
