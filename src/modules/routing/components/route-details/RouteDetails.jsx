import { car, tilt, vertLine } from "../../assets";
import "./RouteDetails.scss";
function RouteDetails({ summary, km, min }) {
  return (
    <div className="detail-card">
      <p className="dir-detail">{summary} </p>
      <div className="duration-container">
        <img src={car} alt="" />
        <div className="duration">
          <div className="num-metric">
            <span className="num">{Math.round(min / 60)}</span>
            <span className="metric">دقیقه</span>
          </div>
          <img src={vertLine} alt="" />
          <div className="num-metric">
            <span className="num">{Math.round(km / 1000)}</span>
            <span className="metric">کیلومتر</span>
          </div>
        </div>
      </div>
      <button className="tilt-btn">
        <img src={tilt} alt="" />
        شیب و جزئیات مسیر
      </button>
    </div>
  );
}

export default RouteDetails;
