import "./Plan.scss";
import { naro, arrow } from "../../assets";
import { useDispatch } from "react-redux";

import { setIsPollution, setIsTraffic } from "../../routingActions";
function Plan({ title, subtitle, naroSrc, boroSrc, active }) {
  const dispatch = useDispatch();

  const handleBoroClick = () => {
    if (title === "طرح ترافیک") dispatch(setIsTraffic(true));
    else if (title === "طرح آلودگی") dispatch(setIsPollution(true));
  };
  const handleNaroClick = () => {
    if (title === "طرح ترافیک") dispatch(setIsTraffic(false));
    else if (title === "طرح آلودگی") dispatch(setIsPollution(false));
  };

  return (
    <div className="plan">
      <div className="plan-text">
        <p className="title">{title}</p>
        <p className="subtitle">{subtitle}</p>
      </div>
      <div className="content-switch">
        <button
          className={`plan-btn ${active ? "active" : ""}`}
          onClick={handleBoroClick}
        >
          <img src={boroSrc} alt="" />
          آره برو
        </button>
        <button
          className={`plan-btn ${!active ? "active" : ""}`}
          style={!active ? { color: "#D64F55" } : {}}
          onClick={handleNaroClick}
        >
          <img src={naroSrc} alt="" />
          نرو
        </button>
      </div>
    </div>
  );
}

export default Plan;
