import "./AvoidTolls.scss";
function AvoidTolls() {
  return (
    <div className="plan avoid-toll">
      <div className="plan-text">
        <p className="title">پرهیز از عوارضی</p>
        <p className="subtitle avoid-tolls">
          <span>سعی میکنیم از عوارضی نریم</span>

          <span> در مسیریابی آفلاین اعمال نمی شود</span>
        </p>
      </div>
      <div className="toggler-wrapper">
        <label className="custom-checkbox ">
          <input type="checkbox" />
          <span className="checkmark"></span>
          <span className="circle-toggle"></span>
        </label>
      </div>
    </div>
  );
}

export default AvoidTolls;
