import { useDispatch, useSelector } from "react-redux";
import { city } from "../../assets";
import "./SearchLocResult.scss";
import { selectDestinationFocus, selectOriginFocus } from "../../routingSelectors";
import { setDestinationCords, setOriginCords } from "../../routingActions";

function SearchLocResult({ title, subtitle, type, cords }) {
  const dispatch = useDispatch();

  const originFocus = useSelector(selectOriginFocus);
  const destinationFocus = useSelector(selectDestinationFocus);

  const handleClick = () => {
    if (originFocus) {
      dispatch(setOriginCords([cords.x, cords.y]));
    }
    if (destinationFocus) {
      dispatch(setDestinationCords([cords.x, cords.y]));
    }
  };

  return (
    <div className="search-loc-result-wrapper" onClick={handleClick}>
      <img src={city} alt="" />
      <div className="search-loc-result-container">
        <p className="title-loc-result">{title}</p>
        <span className="subtitle-loc-result">{subtitle}</span>
        <span className="type-loc-result">{type}</span>
      </div>
    </div>
  );
}

export default SearchLocResult;
