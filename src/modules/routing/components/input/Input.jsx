import { useDispatch, useSelector } from "react-redux";
import "./Input.scss";
import {
  setDestinationFocus,
  setDestinationQuery,
  setOriginFocus,
  setOriginQuery,
} from "../../routingActions";
import useReverseGeo from "../../../info-box/hooks/useReverseGeo";
import { selectDestinationCords, selectOriginCords } from "../../routingSelectors";

function Input({ placeholder }) {
  const dispatch = useDispatch();
  const originCords = useSelector(selectOriginCords);
  const destinationCords = useSelector(selectDestinationCords);
  const { address: originAd } = useReverseGeo(originCords);
  const { address: destAd } = useReverseGeo(destinationCords);
  const handleChange = (e) => {
    if (placeholder === "مبدا") {
      dispatch(setOriginQuery(e.target.value));
    } else if (placeholder === "مقصد") {
      dispatch(setDestinationQuery(e.target.value));
    }
  };
  const handleFocus = () => {
    if (placeholder === "مبدا") {
      dispatch(setOriginFocus(true));
      dispatch(setDestinationFocus(false));
    } else if (placeholder === "مقصد") {
      dispatch(setDestinationFocus(true));
      dispatch(setOriginFocus(false));
    }
  };
  const placeVal =
    placeholder === "مبدا" ? originAd.formatted_address : destAd.formatted_address;

  return (
    <div className={`routing-input-wrapper `}>
      <input
        type="text"
        placeholder={placeholder}
        onChange={(e) => handleChange(e)}
        onFocus={handleFocus}
        value={placeVal}
      />
    </div>
  );
}

export default Input;
