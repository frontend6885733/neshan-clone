import "./Place.scss";

function Place({ icon, text }) {
  return (
    <div className="place">
      <img src={icon} alt={text} />
      <span>{text}</span>
    </div>
  );
}

export default Place;
