import { useDispatch, useSelector } from "react-redux";
import { selectIsExpanded } from "../routingSelectors";
import { setIsExpanded, setMenuPosition } from "../routingActions";

export function setupTouchEvents() {
  const dispatch = useDispatch();
  const isExpanded = useSelector(selectIsExpanded);
  const handleTouchStart = (e) => {
    const startY = e.touches[0].clientY;

    const handleTouchMove = (e) => {
      const deltaY = e.touches[0].clientY - startY;

      if (!isExpanded) {
        if (deltaY <= 0) {
          dispatch(setIsExpanded(true));
          dispatch(setMenuPosition(0));
        }
      } else {
        dispatch(setMenuPosition(deltaY));
        dispatch(setIsExpanded(false));
      }
    };

    const handleTouchEnd = () => {
      window.removeEventListener("touchmove", handleTouchMove);
      window.removeEventListener("touchend", handleTouchEnd);
    };

    window.addEventListener("touchmove", handleTouchMove);
    window.addEventListener("touchend", handleTouchEnd);
  };

  return handleTouchStart;
}

export function setupMouseEvents() {
  const dispatch = useDispatch();
  const isExpanded = useSelector(selectIsExpanded);
  const handleMouseDown = (e) => {
    const startY = e.clientY;

    const handleMouseMove = (e) => {
      const deltaY = e.clientY - startY;

      if (!isExpanded) {
        if (deltaY <= 0) {
          dispatch(setIsExpanded(true));
          dispatch(setMenuPosition(0));
        }
      } else {
        dispatch(setMenuPosition(deltaY));
        dispatch(setIsExpanded(false));
      }
    };

    const handleMouseUp = () => {
      window.removeEventListener("mousemove", handleMouseMove);
      window.removeEventListener("mouseup", handleMouseUp);
    };

    window.addEventListener("mousemove", handleMouseMove);
    window.addEventListener("mouseup", handleMouseUp);
  };

  return handleMouseDown;
}
