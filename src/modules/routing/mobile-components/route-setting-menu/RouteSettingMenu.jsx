import { useDispatch, useSelector } from "react-redux";
import { outlineX } from "../../assets";
import Plans from "../../containers/plans/Plans";
import "./RouteSettingMenu.scss";
import { setShowSettingMenu } from "../../routingActions";
import { selectShowSettingMenu } from "../../routingSelectors";

function RouteSettingMenu() {
  const showSettingMenu = useSelector(selectShowSettingMenu);
  const dispatch = useDispatch();
  return (
    <div className={`route-setting-menu ${showSettingMenu ? "show" : ""}`}>
      <div className="title-sec">
        <span>تنظیمات مسیریابی</span>
        <button onClick={() => dispatch(setShowSettingMenu(false))}>
          <img src={outlineX} alt="close" />
        </button>
      </div>
      <Plans />
    </div>
  );
}

export default RouteSettingMenu;
