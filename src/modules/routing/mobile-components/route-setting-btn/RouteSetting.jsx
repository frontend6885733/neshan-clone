import { useDispatch, useSelector } from "react-redux";
import "./RouteSetting.scss";
import { setShowSettingMenu } from "../../routingActions";
import { setting } from "../../assets";
import { selectShowSettingMenu } from "../../routingSelectors";

function RouteSetting() {
  const dispatch = useDispatch();
  const showSettingMenu = useSelector(selectShowSettingMenu);
  return (
    <button
      className="route-setting-btn"
      onClick={() => dispatch(setShowSettingMenu(true))}
      style={showSettingMenu ? { display: "none" } : {}}
    >
      <img src={setting} alt="" />
      تنظیمات طرح و مسیریابی
    </button>
  );
}

export default RouteSetting;
