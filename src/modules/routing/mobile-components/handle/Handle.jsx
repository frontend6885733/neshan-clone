import "./Handle.scss";
const Handle = () => {
  return (
    <div className="handle-container">
      <div className="handle"></div>
    </div>
  );
};
export default Handle;
