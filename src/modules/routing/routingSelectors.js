export const selectShowSettingMenu = (state) => state.routeUI.showSettingMenu;
export const selectMenuPosition = (state) => state.routeUI.menuPosition;
export const selectIsExpanded = (state) => state.routeUI.isExpanded;
export const selectVehicleType = (state) => state.routeUI.vehicleType;
export const selectIsRoutingOpen = (state) => state.routeUI.isRoutingOpen;
//non-UI selectors
export const selectIsPollution = (state) => state.routing.isPollution;
export const selectIsTraffic = (state) => state.routing.isTraffic;
export const selectOriginQuery = (state) => state.routing.originQuery;
export const selectDestinationQuery = (state) => state.routing.destinationQuery;
export const selectOriginFocus = (state) => state.routing.originFocus;
export const selectDestinationFocus = (state) => state.routing.destinationFocus;
export const selectOriginCords = (state) => state.routing.originCords;
export const selectDestinationCords = (state) => state.routing.destinationCords;
export const selectOriginMarkerRef = (state) => state.routing.originMarkerRef;
export const selectDestMarkerRef = (state) => state.routing.destMarkerRef;
