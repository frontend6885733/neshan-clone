const initialState = {
  showSettingMenu: false,
  menuPosition: 0,
  isExpanded: false,
  vehicleType: "خودرو",
  isRoutingOpen: false,
  isRoute: false,
};
function routingUIReducer(state = initialState, action) {
  switch (action.type) {
    case "ROUTE_SETTING_CLICK":
      return { ...state, showSettingMenu: action.payload };
    case "SET_MENU_POSITION":
      return { ...state, menuPosition: action.payload };
    case "EXPAND_MENU":
      return { ...state, isExpanded: action.payload };
    case "ACTIVE_VEHICLE":
      return { ...state, vehicleType: action.payload };
    case "OPEN_ROUTING_APP":
      return { ...state, isRoutingOpen: action.payload };

    default:
      return state;
  }
}
export default routingUIReducer;
