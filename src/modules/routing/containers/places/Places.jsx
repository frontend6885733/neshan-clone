import Place from "../../components/place/Place";
import "./Places.scss";
import { home, briefcase, star, vertLine } from "../../assets";
function Places() {
  return (
    <>
      <div className="places">
        <Place icon={home} text="خانه" />
        <img src={vertLine} alt="" />
        <Place icon={briefcase} text="محل کار" />
        <img src={vertLine} alt="" />
        <Place icon={star} text="مکان های شخصی" />
      </div>
      <svg
        className="hor-line-places"
        xmlns="http://www.w3.org/2000/svg"
        width="388"
        height="2"
        viewBox="0 0 388 2"
        fill="none"
      >
        <path d="M388 1H0" stroke="#F5F5F5" strokeMiterlimit="10" />
      </svg>
    </>
  );
}

export default Places;
