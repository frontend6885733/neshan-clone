import "./InputField.scss";
import Input from "../../components/input/Input";
import { switchIcon, nav, back, navMobile } from "../../assets";
import useMediaQuery from "../../../../hooks/useMediaQuery";
import map from "../../../../../map";
import {
  setDestinationCords,
  setIsRoutingOpen,
  setOriginCords,
} from "../../routingActions";
import { useDispatch, useSelector } from "react-redux";
import {
  selectDestMarkerRef,
  selectDestinationCords,
  selectOriginCords,
  selectOriginFocus,
  selectOriginMarkerRef,
} from "../../routingSelectors";
import { removeMapLayer } from "../../../../utils/removeMapLayer";
function InputField() {
  const { mobile } = useMediaQuery();
  const dispatch = useDispatch();
  const originFocus = useSelector(selectOriginFocus);
  const originMarkerRef = useSelector(selectOriginMarkerRef);
  const destMarkerRef = useSelector(selectDestMarkerRef);
  const originCords = useSelector(selectOriginCords);
  const destinationCords = useSelector(selectDestinationCords);
  const handleClick = () => {
    dispatch(setIsRoutingOpen(false));
    //reset map
    if (originMarkerRef != null && destMarkerRef != null) {
      originMarkerRef.remove();
      dispatch(setOriginCords([]));
      destMarkerRef.remove();
      dispatch(setDestinationCords([]));
    }
    removeMapLayer();
  };
  const handleSwitchClick = () => {
    const newOrigin = destinationCords;
    const newDest = originCords;
    dispatch(setOriginCords(newOrigin));
    dispatch(setDestinationCords(newDest));
  };
  return (
    <div className="input-field">
      {mobile && (
        <button className="back-btn" onClick={handleClick}>
          <img src={back} alt="back" />
        </button>
      )}
      <img className="nav-icon" src={nav} alt="" />
      {mobile && <img className="mobile-nav-icon" src={navMobile} alt="" />}
      <div className={`inputs-wrapper ${originFocus ? "focus-origin" : "focus-dest"}`}>
        <Input placeholder="مبدا" />
        <div className="input-divider" />
        <Input placeholder="مقصد" />
      </div>
      <button className="switch-wrapper" onClick={handleSwitchClick}>
        <img src={switchIcon} alt="switch" />
      </button>
    </div>
  );
}

export default InputField;
