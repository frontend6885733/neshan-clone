import "./RoutingSidebar.scss";
import VehicleIcons from "../Vehicle-icons/VehicleIcons";
import InputField from "../input-field/InputField";
import Places from "../places/Places";
import Plans from "../plans/Plans";
import useMediaQuery from "../../../../hooks/useMediaQuery";
import Handle from "../../mobile-components/handle/Handle";
import { useSelector } from "react-redux";
import {
  selectDestinationCords,
  selectIsExpanded,
  selectOriginCords,
  selectShowSettingMenu,
} from "../../routingSelectors";
import {
  setupMouseEvents,
  setupTouchEvents,
} from "../../utils/handleTouchAndMouseEvents";
import SearchLocResults from "../Search-loc-results/SearchLocResults";
import AllRouteDetails from "../all-route-details/AllRouteDetails";
import useFetchDirections from "../../hooks/useFetchDirections";

function RoutingSidebar() {
  const originCords = useSelector(selectOriginCords);
  const destinationCords = useSelector(selectDestinationCords);
  //UI Logic
  const { mobile } = useMediaQuery();
  const { route } = useFetchDirections(originCords, destinationCords);

  const showSettingMenu = useSelector(selectShowSettingMenu);
  const isExpanded = useSelector(selectIsExpanded);
  const handleTouchStart = setupTouchEvents();
  const handleMouseDown = setupMouseEvents();
  const sidebarExpand = isExpanded ? "expanded" : "collapsed";
  const hideMobile = showSettingMenu ? "hide" : "";
  return (
    <aside
      className={`routing-sidebar ${hideMobile} ${sidebarExpand} `}
      onTouchStart={handleTouchStart}
      onMouseDown={handleMouseDown}
    >
      {mobile && <Handle />}
      <VehicleIcons />
      {!mobile && (
        <>
          <InputField />
          <Places />
          <Plans />
          <div className="divider" />
        </>
      )}
      {/* if there is a route then display the results */}
      {route && route.routes && route.routes.length > 0 ? (
        <AllRouteDetails />
      ) : (
        <SearchLocResults />
      )}
    </aside>
  );
}

export default RoutingSidebar;
