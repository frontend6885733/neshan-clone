import { useSelector } from "react-redux";
import "./SearchLocResults.scss";
import useFetchLocation from "../../../../hooks/useFetchLocation";
import {
  selectDestinationFocus,
  selectDestinationQuery,
  selectOriginFocus,
  selectOriginQuery,
} from "../../routingSelectors";
import SearchLocResult from "../../components/search-loc-result/SearchLocResult";

function SearchLocResults() {
  const originQuery = useSelector(selectOriginQuery);
  const destinationQuery = useSelector(selectDestinationQuery);
  const originFocus = useSelector(selectOriginFocus);
  const destinationFocus = useSelector(selectDestinationFocus);
  const { location: originResults, isLoading: isLoadingOrigin } =
    useFetchLocation(originQuery);
  const { location: destResults, isLoading: isLoadingDest } =
    useFetchLocation(destinationQuery);

  if (originFocus) {
    return (
      <div className="search-loc-results">
        {originResults?.map((loc, i) => (
          <SearchLocResult
            key={i}
            title={loc.title}
            subtitle={loc.address}
            type={loc.type}
            cords={loc.cord}
          />
        ))}
      </div>
    );
  } else if (destinationFocus) {
    return (
      <div className="search-loc-results">
        {destResults?.map((loc, i) => (
          <SearchLocResult
            key={i}
            title={loc.title}
            subtitle={loc.address}
            type={loc.type}
            cords={loc.cord}
          />
        ))}
      </div>
    );
  }
}

export default SearchLocResults;
