import AvoidTolls from "../../components/avoid-tolls/AvoidTolls";
import Plan from "../../components/plan/Plan";
import "./Plans.scss";
import { selectIsPollution, selectIsTraffic } from "../../routingSelectors";

import { naro, arrow, naroGray, coloredArrow } from "../../assets";
import { useSelector } from "react-redux";

function Plans() {
  const isTraffic = useSelector(selectIsTraffic);
  const isPollution = useSelector(selectIsPollution);

  return (
    <div className="plans">
      <Plan
        title="طرح ترافیک"
        subtitle="از ساعت ۶ تا ۱۶"
        naroSrc={isTraffic ? naroGray : naro}
        boroSrc={isTraffic ? coloredArrow : arrow}
        active={isTraffic ? true : false}
      />
      <Plan
        title="طرح آلودگی"
        subtitle="از ساعت ۶ تا ۱۶"
        naroSrc={isPollution ? naroGray : naro}
        boroSrc={isPollution ? coloredArrow : arrow}
        active={isPollution ? true : false}
      />
      <AvoidTolls />
    </div>
  );
}

export default Plans;
