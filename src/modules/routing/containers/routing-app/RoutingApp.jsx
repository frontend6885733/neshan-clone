import "./RoutingApp.scss";
import RoutingSidebar from "../sidebar/RoutingSidebar";
import useMediaQuery from "../../../../hooks/useMediaQuery";
import InputField from "../input-field/InputField";
import RouteSetting from "../../mobile-components/route-setting-btn/RouteSetting";
import RouteSettingMenu from "../../mobile-components/route-setting-menu/RouteSettingMenu";
import { useDispatch, useSelector } from "react-redux";
import map from "../../../../../map";
import { useEffect } from "react";
// prettier-ignore
import {selectDestMarkerRef,selectDestinationCords,selectDestinationFocus,selectOriginCords,selectOriginFocus,selectOriginMarkerRef,} from "../../routingSelectors";
// prettier-ignore
import { setDestMarkerRef, setDestinationCords, setOriginCords, setOriginMarkerRef,} from "../../routingActions";
import { createMarker } from "../../../../utils/createMarker";
function RoutingApp() {
  const dispatch = useDispatch();
  //selectors
  const originMarkerRef = useSelector(selectOriginMarkerRef);
  const destMarkerRef = useSelector(selectDestMarkerRef);
  const originCords = useSelector(selectOriginCords);
  const destinationCords = useSelector(selectDestinationCords);
  const originFocus = useSelector(selectOriginFocus);
  const destinationFocus = useSelector(selectDestinationFocus);
  //custom hooks
  const { mobile } = useMediaQuery();
  //functions
  const handleMarker = (markerRef, markerLoc, color, setMarkerRef) => {
    if (markerRef) markerRef.remove();
    if (markerLoc.length !== 0) {
      const newMarker = createMarker(markerLoc, color);
      newMarker.on("dragend", () => {
        const newMarkerLoc = newMarker.getLngLat();
        if (markerLoc === originCords) {
          dispatch(setOriginCords([newMarkerLoc.lng, newMarkerLoc.lat]));
        } else if (markerLoc === destinationCords) {
          dispatch(setDestinationCords([newMarkerLoc.lng, newMarkerLoc.lat]));
        }
      });

      dispatch(setMarkerRef(newMarker));
    }
  };
  //put a marker using click on the map
  const handleClick = (e) => {
    if (originFocus) {
      const clickedMarkerLoc = [e.lngLat.lng, e.lngLat.lat];
      dispatch(setOriginCords(clickedMarkerLoc));
    } else if (destinationFocus) {
      const clickedMarkerLoc = [e.lngLat.lng, e.lngLat.lat];
      dispatch(setDestinationCords(clickedMarkerLoc));
    }
  };
  useEffect(() => {
    handleMarker(originMarkerRef, originCords, "#6f6fec", setOriginMarkerRef);
    handleMarker(destMarkerRef, destinationCords, "#f05e60", setDestMarkerRef);
  }, [originCords, destinationCords]);

  useEffect(() => {
    map.on("click", handleClick);
    return () => map.off("click", handleClick);
  }, [originFocus]);

  return (
    <>
      {mobile && <InputField />}
      {mobile && <RouteSetting />}
      {mobile && <RouteSettingMenu />}
      <RoutingSidebar />
    </>
  );
}

export default RoutingApp;
