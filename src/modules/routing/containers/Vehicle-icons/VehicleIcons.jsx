import "./VehicleIcons.scss";
import VehicleIcon from "../../components/vehicle-icon/VehicleIcon";
//prettier-ignore
import { car, bike, bus, motor, walk, x, carBlue, bikeBlue, busBlue, motorBlue, walkBlue,} from "../../assets";
import { useDispatch, useSelector } from "react-redux";
import {
  setDestinationCords,
  setIsRoutingOpen,
  setOriginCords,
} from "../../routingActions";
import { selectDestMarkerRef, selectOriginMarkerRef } from "../../routingSelectors";
import { removeMapLayer } from "../../../../utils/removeMapLayer";
function VehicleIcons() {
  const dispatch = useDispatch();

  const originMarkerRef = useSelector(selectOriginMarkerRef);
  const destMarkerRef = useSelector(selectDestMarkerRef);
  const handleClick = () => {
    dispatch(setIsRoutingOpen(false));
    //reset map
    if (originMarkerRef != null && destMarkerRef != null) {
      originMarkerRef.remove();
      dispatch(setOriginCords([]));
      destMarkerRef.remove();
      dispatch(setDestinationCords([]));
    }
    removeMapLayer();
  };

  return (
    <div className="vehicle-icon-section">
      <div className="vehicle-iconset">
        <VehicleIcon src={car} coloredSrc={carBlue} type="خودرو" />
        <VehicleIcon src={bus} coloredSrc={busBlue} type="همگانی" />
        <VehicleIcon src={walk} coloredSrc={walkBlue} type="پیاده" />
        <VehicleIcon src={bike} coloredSrc={bikeBlue} type="دوچرخه" />
        <VehicleIcon src={motor} coloredSrc={motorBlue} type="موتور" />
      </div>
      <button className="x-icon-wrapper" onClick={handleClick}>
        <img src={x} alt="close" />
      </button>
    </div>
  );
}

export default VehicleIcons;
