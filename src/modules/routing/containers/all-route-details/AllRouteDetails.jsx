import { useDispatch, useSelector } from "react-redux";
import RouteDetails from "../../components/route-details/RouteDetails";
import useFetchDirections from "../../hooks/useFetchDirections";
import { selectDestinationCords, selectOriginCords } from "../../routingSelectors";
import { createRouteLayer } from "../../../../utils/createRouteLayer";

function AllRouteDetails() {
  const originCords = useSelector(selectOriginCords);
  const destinationCords = useSelector(selectDestinationCords);
  const { route } = useFetchDirections(originCords, destinationCords);

  if (route && route.routes && route.routes.length != 0) {
    const polyline = route.routes[0].overview_polyline.points;
    createRouteLayer(polyline);
  }

  return (
    <>
      {route.routes &&
        route.routes.map((item, i) => (
          <RouteDetails
            key={i}
            summary={item.legs[0].summary}
            km={item.legs[0].distance.value}
            min={item.legs[0].duration.value}
          />
        ))}
    </>
  );
}

export default AllRouteDetails;
