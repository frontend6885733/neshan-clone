const initialState = {
  isTraffic: true,
  isPollution: true,
  originQuery: "",
  destinationQuery: "",
  originFocus: true,
  originCords: [],
  destinationCords: [],
  originMarkerRef: null,
  destMarkerRef: null,
};
function routingReducer(state = initialState, action) {
  switch (action.type) {
    case "IS_Traffic":
      return { ...state, isTraffic: action.payload };
    case "IS_POLLUTION":
      return { ...state, isPollution: action.payload };
    case "SET_ORIGIN_QUERY":
      return { ...state, originQuery: action.payload };
    case "SET_DESTINATION_QUERY":
      return { ...state, destinationQuery: action.payload };
    case "ORIGIN_INPUT_IS_ON_FOCUS":
      return { ...state, originFocus: action.payload };
    case "DESTINATION_INPUT_IS_ON_FOCUS":
      return { ...state, destinationFocus: action.payload };
    case "SET_ORIGIN_CORDS":
      return { ...state, originCords: action.payload };
    case "SET_DESTINATION_CORDS":
      return { ...state, destinationCords: action.payload };
    case "SET_ORIGIN_MARKER_REF":
      return { ...state, originMarkerRef: action.payload };
    case "SET_DESTINATION_MARKER_REF":
      return { ...state, destMarkerRef: action.payload };

    default:
      return state;
  }
}
export default routingReducer;
