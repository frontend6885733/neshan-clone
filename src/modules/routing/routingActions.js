export const setShowSettingMenu = (value) => {
  return { type: "ROUTE_SETTING_CLICK", payload: value };
};
export const setMenuPosition = (position) => {
  return { type: "SET_MENU_POSITION", payload: position };
};
export const setIsExpanded = (value) => {
  return { type: "EXPAND_MENU", payload: value };
};
export const setVehicleType = (value) => {
  return { type: "ACTIVE_VEHICLE", payload: value };
};
export const setIsRoutingOpen = (value) => {
  return { type: "OPEN_ROUTING_APP", payload: value };
};

//non UI Actions
export const setIsTraffic = (value) => {
  return { type: "IS_Traffic", payload: value };
};
export const setIsPollution = (value) => {
  return { type: "IS_POLLUTION", payload: value };
};
export const setOriginQuery = (value) => {
  return { type: "SET_ORIGIN_QUERY", payload: value };
};
export const setDestinationQuery = (value) => {
  return { type: "SET_DESTINATION_QUERY", payload: value };
};
export const setOriginFocus = (value) => {
  return { type: "ORIGIN_INPUT_IS_ON_FOCUS", payload: value };
};
export const setDestinationFocus = (value) => {
  return { type: "DESTINATION_INPUT_IS_ON_FOCUS", payload: value };
};
export const setOriginCords = (value) => {
  return { type: "SET_ORIGIN_CORDS", payload: value };
};
export const setDestinationCords = (value) => {
  return { type: "SET_DESTINATION_CORDS", payload: value };
};
export const setOriginMarkerRef = (value) => {
  return { type: "SET_ORIGIN_MARKER_REF", payload: value };
};
export const setDestMarkerRef = (value) => {
  return { type: "SET_DESTINATION_MARKER_REF", payload: value };
};
