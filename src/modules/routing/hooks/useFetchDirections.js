import { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { selectIsTraffic, selectVehicleType } from "../routingSelectors";
const useFetchDirections = (originCords, destinationCords) => {
  const vehicleType = useSelector(selectVehicleType);
  const [route, setRoute] = useState([]);
  const isTraffic = useSelector(selectIsTraffic);
  let vehicle;
  if (vehicleType === "خودرو") vehicle = "car";
  else if (vehicleType === "موتور") vehicle = "motorcycle";
  const traffic = isTraffic ? "/no-traffic" : "";
  useEffect(() => {
    async function fetchData() {
      if (originCords.length !== 0 && destinationCords.length !== 0) {
        const response = await fetch(
          `https://api.neshan.org/v4/direction${traffic}?type=${vehicle}&origin=${originCords[1]},${originCords[0]}&destination=${destinationCords[1]},${destinationCords[0]}`,
          {
            headers: {
              "Api-Key": "service.ca0fbe3c740a4461a17d7000d4461c76",
            },
          }
        );
        const data = await response.json();
        setRoute(data);
      }
    }
    fetchData();
  }, [originCords, destinationCords, isTraffic, vehicle]);

  return { route };
};
export default useFetchDirections;
