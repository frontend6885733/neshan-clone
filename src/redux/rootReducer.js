import { combineReducers } from "redux";
import routingReducer from "../modules/routing/routingReducer";

import routingUIReducer from "../modules/routing/routingUIReducer";
import infoBoxUIReducer from "../modules/info-box/infoBoxUIReducer";
import infoBoxReducer from "../modules/info-box/infoBoxReducer";
const rootReducer = combineReducers({
  routing: routingReducer,
  info: infoBoxReducer,
  routeUI: routingUIReducer,
  infoUI: infoBoxUIReducer,
});

export default rootReducer;
