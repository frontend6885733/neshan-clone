import map from "../../map";
export function removeMapLayer() {
  const sourceId = "LineString";
  const existingSource = map.getSource(sourceId);
  if (existingSource) {
    map.removeLayer("LineString");
  }
}
