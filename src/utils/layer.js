export const layer = {
  id: "LineString",
  type: "line",
  source: "LineString",
  layout: {
    "line-join": "round",
    "line-cap": "round",
  },
  paint: {
    "line-color": "#8A1EF7",
    "line-width": 5,
  },
};
