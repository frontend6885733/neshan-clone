import mapboxgl from "mapbox-gl";
export const createMap = () => {
  const mapRef = new mapboxgl.Map({
    container: "map",
    style: "mapbox://styles/mapbox/streets-v12",
    center: [59.578749, 36.313287],
    zoom: 10,
  });

  return mapRef;
};
