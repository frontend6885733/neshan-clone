import map from "../../map";
import * as polyline from "@mapbox/polyline";
import { layer } from "./layer";
import mapboxgl from "mapbox-gl";
export function createRouteLayer(encodedPolyline) {
  const cord = polyline.decode(encodedPolyline);
  const coordinates = cord.map(([lat, lon]) => [lon, lat]);
  const geojson = {
    type: "Feature",
    properties: {},
    geometry: {
      type: "LineString",
      coordinates: coordinates,
    },
  };

  const sourceId = "LineString";
  const existingSource = map.getSource(sourceId);
  if (!existingSource) {
    map.addSource(sourceId, {
      type: "geojson",
      data: geojson,
    });
  } else {
    map.getSource(sourceId).setData(geojson);
  }
  const existingLayer = map.getLayer("LineString");

  if (!existingLayer) {
    map.addLayer(layer);
  }

  // Fit the map to the bounds of the route
  const bounds = geojson.geometry.coordinates.reduce(
    (bounds, coord) => bounds.extend(coord),
    new mapboxgl.LngLatBounds(
      geojson.geometry.coordinates[0],
      geojson.geometry.coordinates[0]
    )
  );
  map.fitBounds(bounds, {
    padding: { top: 50, bottom: 100, left: 100, right: 100 },
  });
}
