import mapboxgl from "mapbox-gl";
import map from "../../map";

export function createMarker(position, color) {
  const marker = new mapboxgl.Marker({
    color: color,
    draggable: true,
  })
    .setLngLat(position)
    .addTo(map);
  map.flyTo({
    center: [position[0], position[1]],
    zoom: 10,
  });

  return marker;
}
