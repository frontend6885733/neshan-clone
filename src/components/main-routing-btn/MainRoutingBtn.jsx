import "./MainRoutingBtn.scss";
import dirIcon from "../../assets/2dir.svg";
import { useDispatch, useSelector } from "react-redux";
import { setIsRoutingOpen } from "../../modules/routing/routingActions";
import { selectIsRoutingOpen } from "../../modules/routing/routingSelectors";
function MainRoutingBtn() {
  const dispatch = useDispatch();
  const isRoutingOpen = useSelector(selectIsRoutingOpen);
  return (
    <>
      {!isRoutingOpen && (
        <button
          className="main-routing-btn"
          onClick={() => dispatch(setIsRoutingOpen(true))}
        >
          <img src={dirIcon} alt="routing" />
        </button>
      )}
    </>
  );
}

export default MainRoutingBtn;
