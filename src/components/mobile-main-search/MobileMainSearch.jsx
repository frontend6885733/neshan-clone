import { useDispatch } from "react-redux";
import { search } from "../../modules/info-box/assets";
import "./MobileMainSearch.scss";
import { setIsMobileSearch } from "../../modules/info-box/infoBoxActions";

function MobileMainSearch() {
  const dispatch = useDispatch();
  return (
    <div className="mobile-main-input-wrapper">
      <input
        placeholder="کجا می‌خوای بری؟"
        className="mobile-main-input"
        type="search"
        autoComplete="off"
        inputMode="search"
        onFocus={() => dispatch(setIsMobileSearch(true))}
      ></input>
      <img src={search} alt="" />
    </div>
  );
}

export default MobileMainSearch;
